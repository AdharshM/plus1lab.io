---
path: /blog/first-post
date: "2019-11-05"
cover: "./manmount.jpg"
tag: "first-post"
author: "abbyck"
name: "Abhinav Krishna C K"
title: "A new face for PLUS. (Website redesign)"
desc: "A welcome post"
---

# Welcome

This is the Test blog post. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam quis tortor lacinia nunc imperdiet aliquam. Duis ante orci, porta non libero ut, luctus convallis enim. Duis quis finibus lorem, ut dignissim lorem. Nulla nec aliquam dui, et aliquam enim. Praesent id eros bibendum, feugiat orci eu, venenatis mauris. Duis vestibulum erat vitae lectus tempus luctus. Nam malesuada, mi id tincidunt interdum, justo ligula ultrices velit, eget sodales nisl turpis a turpis. Etiam vestibulum urna et ligula lobortis, a sodales ligula feugiat. Nunc a rutrum justo, eget fermentum tellus.

![a man looking at mountain](./manmount.jpg)

Sed consequat sapien eget justo accumsan, sit amet posuere elit dignissim. Vestibulum pellentesque enim in elementum lobortis. Etiam ut lorem id diam accumsan varius in vel leo. In eu ultrices diam, eget venenatis turpis. Mauris vitae dolor non risus efficitur imperdiet. Sed gravida, enim in suscipit faucibus, quam nisl 

```javascript
if(this){
	print("Hello World");
}
```

Drdum volutpat dui. Curabitur massa sem, auctor sit amet condimentum id, placerat eu lorem. Vivamus vitae sem cursus, dictum felis ac, pretium neque. Praesent sem nisl, efficitur eget enim ac, lacinia maximus ligula. Cras semper nunc ut orci facilisis scelerisque. Etiam placerat dui a nunc pulvinar interdum. Mauris lacus ligula, maximus eu elit vitae, consequat sollicitudin nisi. In dictum eros et fringilla scelerisque. Donec et tellus enim.

1. This is a starting list item
2. This is the second item
3. What am I capable of doing?
